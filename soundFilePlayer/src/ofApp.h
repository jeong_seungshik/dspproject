#pragma once

#include "ofMain.h"
#include "ofxPortSF.h"
#include "audioTools.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
		void exit();
		void audioOut(float* buffer, int bufferSize, int nChannels);

		void keyPressed(int key);
		void mouseMoved(int x, int y );
		void  convertToMono();


		//Used for loading sound
		PSF_PROPS soundFileProperties;//will be auto-populated when file loaded
		long framesRead, totalFramesRead;//framesread used to check if there are still more frames in file
		int ifd = -1;//identification for the sound file, used by the library
		float* frame = NULL;//location of currently read frame
		vector<float> soundFile;//actual stored sound file
		vector<float> monoSummedSoundFile;
		//used for playing sound
		bool playSound;//on/off toggle
		float readPoint;//location of needle head
		float readSpeed;

		int sampleRate;

		enum {
			TRUNCATE,
			LINEAR,
			CUBIC
		};

		int currentInterpMode;

		Crossover crossover;
		float crossoverFreq[3];
		float crossoverVolume[4];
};
