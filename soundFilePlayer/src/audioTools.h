#ifndef __AUDIOTOOLS_H__
#define __AUDIOTOOLS_H__

#include "ofMain.h"

class oscillator {

public:
	void setup(float _frequency, double _phase, float _amplitude, int _sampleRate);

	float getSample();
	void setFrequency(float _frequency);
	void setAmplitude(float _amplitude);

private:
	int sampleRate;

	float frequency;
	double phase;
	double phaseInc;
	float amplitude;

	float currentSample;
};

struct stereoFrame {
	float leftSample;
	float rightSample;
};

class panner {

public:
	stereoFrame pan(float input, float position);
private:
	const float sqrt2div2 = (sqrt(2) / (float)2);
};

class parameterSmoother {

public:

	parameterSmoother(float smoothingTimeMS, int sampleRate) {
		a = exp(-TWO_PI / (smoothingTimeMS * 0.001 * sampleRate));
		b = 1.0 - a;
		z = 0.0;
	}

	inline float process(float input) {
		z = (input * b) + (z * a);
		return z;
	}
private:
	float a;
	float b;
	float z;
};

struct smoothValue {
	float targetValue;
	float currentValue;
	parameterSmoother* smoother;
};

inline float linearInterp(float y0, float y1, float currentX) {
	float interpolatedValue;
	float scaleValue = currentX - (int)currentX;

	interpolatedValue = (y0 * (1 - scaleValue)) + (y1 * scaleValue);

	return interpolatedValue;
}

inline float cubicInterp(float y0, float y1, float y2, float y3, float currentX) {

	float interpolatedValue;

	float a = (-0.5 * y0) + (1.5 * y1) - (1.5 * y2) + (0.5 * y3);
	float b = y0 - (2.5 * y1) + (2.0 * y2) - (0.5 * y3);
	float c = (-0.5 * y0) + (0.5 * y2);
	float d = y1;

	float xOff = currentX - (int)currentX;
	interpolatedValue = (a * pow(xOff, 3)) + b * (pow(xOff, 2)) + (c * xOff) + d;

	return interpolatedValue;
}

//
//  Biquad.h
//
//  Created by Nigel Redmon on 11/24/12
//  EarLevel Engineering: earlevel.com
//  Copyright 2012 Nigel Redmon
//
//  For a complete explanation of the Biquad code:
//  http://www.earlevel.com/main/2012/11/26/biquad-c-source-code/
//
//  License:
//
//  This source code is provided as is, without warranty.
//  You may copy and distribute verbatim copies of this document.
//  You may modify and use this source code to create binary code
//  for your own purposes, free or commercial.
//

enum {
	bq_type_lowpass = 0,
	bq_type_highpass,
	bq_type_bandpass,
	bq_type_notch,
	bq_type_peak,
	bq_type_lowshelf,
	bq_type_highshelf
};

class Biquad {
public:
	Biquad();
	Biquad(int type, double Fc, double Q, double peakGainDB);
	~Biquad();
	void setType(int type);
	void setQ(double Q);
	void setFc(double Fc);
	void setPeakGain(double peakGainDB);
	void setBiquad(int type, double Fc, double Q, double peakGainDB);
	float process(float in);

protected:
	void calcBiquad(void);

	int type;
	double a0, a1, a2, b1, b2;
	double Fc, Q, peakGain;
	double z1, z2;
};

inline float Biquad::process(float in) {
	double out = in * a0 + z1;
	z1 = in * a1 + z2 - b1 * out;
	z2 = in * a2 - b2 * out;
	return out;
}

class Crossover {
public:
	enum {
		xf_low,
		xf_mid,
		xf_high
	};

	enum {
		xv_low,
		xv_mid,
		xv_midhigh,
		xv_high
	};

	Biquad band[4]; // low, mid, mid-high, high

	void setup(int sampleRate);
	float process(float in);
	float setFc(int _band, float _freq);
	float setVolume(int _band, float _volume);
private:
	float crossFreq[5];
	float volume[4];
	int sampleRate;
};

#endif // __AUDIOTOOLS_H__