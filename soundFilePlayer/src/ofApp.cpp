#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {

	if (psf_init()) {//initialize the portSF library
		cout << "unable to initalize port sound file ):" << endl;//warn if initialization fails
	}
	
	sampleRate = 48000;

	playSound = false;//dont try to play until sound is loaded
	readPoint = 0;//start sound from beginning 
	readSpeed = 1;
	//start audio thread
	//ofSoundStreamSetup(6, 0, 48000, 512, 6);//5.1
	
	crossoverFreq[0] = 100;
	crossoverFreq[1] = 300;
	crossoverFreq[2] = 4000;

	crossoverVolume[0] = 50;
	crossoverVolume[1] = 50;
	crossoverVolume[2] = 50;
	crossoverVolume[3] = 50;

	crossover.setup(sampleRate);

	for (int i = 0; i < 3; ++i) {
		crossoverFreq[i] = crossover.setFc(i + 1, crossoverFreq[i]);
	}

	for (int i = 0; i < 4; ++i) {
		crossoverVolume[i] = crossover.setVolume(i, crossoverVolume[i]);
	}

	currentInterpMode = TRUNCATE;
	ofSoundStreamSetup(2, 0, 48000, 512, 4);//stereo
}

//--------------------------------------------------------------
void ofApp::update() {

}

//--------------------------------------------------------------
void ofApp::draw() {
	ofBackground(50);
	ofDrawBitmapString("press spacebar to load sound", 20, 20);
	ofDrawBitmapString("sound will start playing once loaded", 20, 40);
	ofDrawBitmapString("1 for truncate, 2 for linear, 3 for cubic interpolation", 20, 60);

}
//--------------------------------------------------------------
void ofApp::audioOut(float* buffer, int bufferSize, int nChannels) {
	for (int i = 0; i < bufferSize; i++) {

		float currentSample = 0;
		float y0, y1, y2, y3;

		if (playSound) {
			
			switch (currentInterpMode) {

			case TRUNCATE:
				currentSample = monoSummedSoundFile[(int)readPoint];
				break;
			case LINEAR:
				y0 = monoSummedSoundFile[(int)readPoint];
				if (readPoint < monoSummedSoundFile.size() - 2) {
					y1 = monoSummedSoundFile[(int)readPoint + 1];

				}
				else {
					y1 = monoSummedSoundFile[(int)readPoint];
				}

				currentSample = linearInterp(y0, y1, readPoint);
				break;
			case CUBIC:
				if (readPoint > 1) {
					y0 = monoSummedSoundFile[(int)readPoint - 1];
				}
				else {
					y0 = monoSummedSoundFile[(int)readPoint];
				}

				y1 = monoSummedSoundFile[(int)readPoint];

				if (readPoint < monoSummedSoundFile.size() - 2) {
					y2 = monoSummedSoundFile[(int)readPoint + 1];
				}
				else {
					y2 = monoSummedSoundFile[(int)readPoint];
				}

				if (readPoint < monoSummedSoundFile.size() - 3) {
					y3 = monoSummedSoundFile[(int)readPoint + 2];
				}
				else if (readPoint < monoSummedSoundFile.size() - 2) {
					y3 = monoSummedSoundFile[(int)readPoint + 1];
				}
				else {
					y3 = monoSummedSoundFile[(int)readPoint];
				}

				currentSample = cubicInterp(y0, y1, y2, y3, readPoint);
				break;
			default:
				currentSample = 0;
				break;
			}

			readPoint += readSpeed;

			if (readPoint >= monoSummedSoundFile.size() - 1) {
				readPoint = 0;
			}
			else if (readPoint < 0) {
				readPoint = monoSummedSoundFile.size() - 1;
			}

			currentSample = crossover.process(currentSample);

			currentSample *= 0.3;

			buffer[i * nChannels + 0] = currentSample;
			buffer[i * nChannels + 1] = currentSample;

		}

	}
}
//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
	if (key == ' ') {

		ofFileDialogResult loadFileResult = ofSystemLoadDialog();//open the load dialog
		string path = loadFileResult.getPath();//store the string
		const char* filePath = path.c_str();// convert from fancy string to C string

		ifd = psf_sndOpen(filePath, &soundFileProperties, 0);//open the sound file

		if (ifd < 0) {//this can happen if the file does not exist or the format is not supported
			ofSystemAlertDialog("Error loading file");
		}
		else {
			frame = (float*)malloc(soundFileProperties.chans * sizeof(float));//set the size of frame to be the nSamples per Frame
			framesRead = psf_sndReadFloatFrames(ifd, frame, 1);//grab the first frame
			totalFramesRead = 0;
			soundFile.clear();//clear the vector that the soundFile will be stored in
			while (framesRead == 1) {//while there are still frames to be read
				totalFramesRead++;//keep track of number of frames;
				for (int i = 0; i < soundFileProperties.chans; i++) {//for every sample in the frame
					soundFile.push_back(frame[i]);//add frame to the soundFile vector
				}
				framesRead = psf_sndReadFloatFrames(ifd, frame, 1);//returns 1 if there are still more frames
				//cout << framesRead << " " << endl;
			}

			convertToMono();

			readPoint = 0;//start sound from beginning
			playSound = true;//start playing the sound once loaded
		}
	}

	switch (key) {
		
	case '1':
		currentInterpMode = TRUNCATE;
		break;
	case '2':
		currentInterpMode = LINEAR;
		break;
	case '3':
		currentInterpMode = CUBIC;
		break;
	case 'q':
		crossoverFreq[0] = crossover.setFc(1, crossoverFreq[0] + 100);
		printf("crossoverFreq[1] = %.0f\n", crossoverFreq[0]);
		break;
	case 'w':
		crossoverFreq[1] = crossover.setFc(2, crossoverFreq[1] + 100);
		printf("crossoverFreq[2] = %.0f\n", crossoverFreq[1]);
		break;
	case 'e':
		crossoverFreq[2] = crossover.setFc(3, crossoverFreq[2] + 100);
		printf("crossoverFreq[3] = %.0f\n", crossoverFreq[2]);
		break;
	case 'a':
		crossoverFreq[0] = crossover.setFc(1, crossoverFreq[0] - 100);
		printf("crossoverFreq[1] = %.0f\n", crossoverFreq[0]);
		break;
	case 's':
		crossoverFreq[1] = crossover.setFc(2, crossoverFreq[1] - 100);
		printf("crossoverFreq[2] = %.0f\n", crossoverFreq[1]);
		break;
	case 'd':
		crossoverFreq[2] = crossover.setFc(3, crossoverFreq[2] - 100);
		printf("crossoverFreq[3] = %.0f\n", crossoverFreq[2]);
		break;
	case 'y':
		crossoverVolume[0] = crossover.setVolume(0, crossoverVolume[0] + 10);
		printf("crossoverVolume[0] = %.0f\n", crossoverVolume[0]);
		break;
	case 'u':
		crossoverVolume[1] = crossover.setVolume(1, crossoverVolume[1] + 10);
		printf("crossoverVolume[1] = %.0f\n", crossoverVolume[1]);
		break;
	case 'i':
		crossoverVolume[2] = crossover.setVolume(2, crossoverVolume[2] + 10);
		printf("crossoverVolume[2] = %.0f\n", crossoverVolume[2]);
		break;
	case 'o':
		crossoverVolume[3] = crossover.setVolume(3, crossoverVolume[3] + 10);
		printf("crossoverVolume[3] = %.0f\n", crossoverVolume[3]);
		break;
	case 'h':
		crossoverVolume[0] = crossover.setVolume(0, crossoverVolume[0] - 10);
		printf("crossoverVolume[0] = %.0f\n", crossoverVolume[0]);
		break;
	case 'j':
		crossoverVolume[1] = crossover.setVolume(1, crossoverVolume[1] - 10);
		printf("crossoverVolume[1] = %.0f\n", crossoverVolume[1]);
		break;
	case 'k':
		crossoverVolume[2] = crossover.setVolume(2, crossoverVolume[2] - 10);
		printf("crossoverVolume[2] = %.0f\n", crossoverVolume[2]);
		break;
	case 'l':
		crossoverVolume[3] = crossover.setVolume(3, crossoverVolume[3] - 10);
		printf("crossoverVolume[3] = %.0f\n", crossoverVolume[3]);
		break;
	default:
		if (key != ' ') {
			cout << "key input not supported" << endl;
		}
		break;
	}

	
}

//--------------------------------------------------------------
void ofApp::exit() {
	ofSoundStreamClose();
	psf_finish();//safety. will close sound files on exit
}

void ofApp::mouseMoved(int x, int y) {
}

void ofApp::convertToMono() {

	monoSummedSoundFile.clear();
	for (int i = 0; i < soundFile.size(); i += soundFileProperties.chans) {
		float currentFrameSum = 0;
		for (int j = 0; j < soundFileProperties.chans; ++j) {
			currentFrameSum += soundFile[i + j] * 0.7;	// frame + channel
		}
		monoSummedSoundFile.push_back(currentFrameSum);
	}
}