#include "audioTools.h"

void oscillator::setup(float _frequency, double _phase, float _amplitude, int _sampleRate) {
	frequency = _frequency;
	phase = _phase;
	amplitude = _amplitude;
	sampleRate = _sampleRate;
	phaseInc = (TWO_PI * frequency) / (double)sampleRate;
	currentSample = 0;
}

float oscillator::getSample() {
	currentSample = sin(phase) * amplitude;
	phase += phaseInc;

	return currentSample;
}

void oscillator::setFrequency(float _frequency) {
	frequency = _frequency;
	phaseInc = (TWO_PI * frequency) / (double)sampleRate;
}

void oscillator::setAmplitude(float _amplitude) {
	amplitude = _amplitude;
}

stereoFrame panner::pan(float input, float position) {
	stereoFrame currentFrame;

	if (position > 1) {
		position = 1;
	}
	else if (position < -1) {
		position = -1;
	}

	float theta = ofDegToRad(position * 45);

	float sinTheta = sin(theta);
	float cosTheta = cos(theta);

	currentFrame.leftSample = input * sqrt2div2 * (sinTheta - cosTheta);
	currentFrame.rightSample = input * sqrt2div2 * (sinTheta + cosTheta);

	return currentFrame;
}

Biquad::Biquad() {
    type = bq_type_lowpass;
    a0 = 1.0;
    a1 = a2 = b1 = b2 = 0.0;
    Fc = 0.50;
    Q = 0.707;
    peakGain = 0.0;
    z1 = z2 = 0.0;
}

Biquad::Biquad(int type, double Fc, double Q, double peakGainDB) {
    setBiquad(type, Fc, Q, peakGainDB);
    z1 = z2 = 0.0;
}

Biquad::~Biquad() {
}

void Biquad::setType(int type) {
    this->type = type;
    calcBiquad();
}

void Biquad::setQ(double Q) {
    this->Q = Q;
    calcBiquad();
}

void Biquad::setFc(double Fc) {
    this->Fc = Fc;
    calcBiquad();
}

void Biquad::setPeakGain(double peakGainDB) {
    this->peakGain = peakGainDB;
    calcBiquad();
}

void Biquad::setBiquad(int type, double Fc, double Q, double peakGainDB) {
    this->type = type;
    this->Q = Q;
    this->Fc = Fc;
    setPeakGain(peakGainDB);
}

void Biquad::calcBiquad(void) {
    double norm;
    double V = pow(10, fabs(peakGain) / 20.0);
    double K = tan(PI * Fc);
    switch (this->type) {
    case bq_type_lowpass:
        norm = 1 / (1 + K / Q + K * K);
        a0 = K * K * norm;
        a1 = 2 * a0;
        a2 = a0;
        b1 = 2 * (K * K - 1) * norm;
        b2 = (1 - K / Q + K * K) * norm;
        break;

    case bq_type_highpass:
        norm = 1 / (1 + K / Q + K * K);
        a0 = 1 * norm;
        a1 = -2 * a0;
        a2 = a0;
        b1 = 2 * (K * K - 1) * norm;
        b2 = (1 - K / Q + K * K) * norm;
        break;

    case bq_type_bandpass:
        norm = 1 / (1 + K / Q + K * K);
        a0 = K / Q * norm;
        a1 = 0;
        a2 = -a0;
        b1 = 2 * (K * K - 1) * norm;
        b2 = (1 - K / Q + K * K) * norm;
        break;

    case bq_type_notch:
        norm = 1 / (1 + K / Q + K * K);
        a0 = (1 + K * K) * norm;
        a1 = 2 * (K * K - 1) * norm;
        a2 = a0;
        b1 = a1;
        b2 = (1 - K / Q + K * K) * norm;
        break;

    case bq_type_peak:
        if (peakGain >= 0) {    // boost
            norm = 1 / (1 + 1 / Q * K + K * K);
            a0 = (1 + V / Q * K + K * K) * norm;
            a1 = 2 * (K * K - 1) * norm;
            a2 = (1 - V / Q * K + K * K) * norm;
            b1 = a1;
            b2 = (1 - 1 / Q * K + K * K) * norm;
        }
        else {    // cut
            norm = 1 / (1 + V / Q * K + K * K);
            a0 = (1 + 1 / Q * K + K * K) * norm;
            a1 = 2 * (K * K - 1) * norm;
            a2 = (1 - 1 / Q * K + K * K) * norm;
            b1 = a1;
            b2 = (1 - V / Q * K + K * K) * norm;
        }
        break;
    case bq_type_lowshelf:
        if (peakGain >= 0) {    // boost
            norm = 1 / (1 + sqrt(2) * K + K * K);
            a0 = (1 + sqrt(2 * V) * K + V * K * K) * norm;
            a1 = 2 * (V * K * K - 1) * norm;
            a2 = (1 - sqrt(2 * V) * K + V * K * K) * norm;
            b1 = 2 * (K * K - 1) * norm;
            b2 = (1 - sqrt(2) * K + K * K) * norm;
        }
        else {    // cut
            norm = 1 / (1 + sqrt(2 * V) * K + V * K * K);
            a0 = (1 + sqrt(2) * K + K * K) * norm;
            a1 = 2 * (K * K - 1) * norm;
            a2 = (1 - sqrt(2) * K + K * K) * norm;
            b1 = 2 * (V * K * K - 1) * norm;
            b2 = (1 - sqrt(2 * V) * K + V * K * K) * norm;
        }
        break;
    case bq_type_highshelf:
        if (peakGain >= 0) {    // boost
            norm = 1 / (1 + sqrt(2) * K + K * K);
            a0 = (V + sqrt(2 * V) * K + K * K) * norm;
            a1 = 2 * (K * K - V) * norm;
            a2 = (V - sqrt(2 * V) * K + K * K) * norm;
            b1 = 2 * (K * K - 1) * norm;
            b2 = (1 - sqrt(2) * K + K * K) * norm;
        }
        else {    // cut
            norm = 1 / (V + sqrt(2 * V) * K + K * K);
            a0 = (1 + sqrt(2) * K + K * K) * norm;
            a1 = 2 * (K * K - 1) * norm;
            a2 = (1 - sqrt(2) * K + K * K) * norm;
            b1 = 2 * (K * K - V) * norm;
            b2 = (V - sqrt(2 * V) * K + K * K) * norm;
        }
        break;
    }

    return;
}

void Crossover::setup(int _sampleRate) {

    sampleRate = _sampleRate;

    crossFreq[0] = -1;
    crossFreq[1] = 100;
    crossFreq[2] = 320;
    crossFreq[3] = 4000;
    crossFreq[4] = 22001;

    band[0].setBiquad(bq_type_lowpass, 100 / (float)sampleRate, 0.707, -3);
    band[1].setBiquad(bq_type_bandpass, 320 / (float)sampleRate, 0.707, -3);
    band[2].setBiquad(bq_type_bandpass, 3000 / (float)sampleRate, 0.707, -3);
    band[3].setBiquad(bq_type_highpass, 4000 / (float)sampleRate, 0.707, -3);

}

float Crossover::process(float in) {
    float sample = 0;
    for (int i = 0; i < 4; ++i) {
        sample += band[i].process(in) * (volume[i] / 100.0);
    }

    return sample;
}

float Crossover::setFc(int _band, float freq) {
    if (freq >= 0 && freq <= 22000) {
        if (crossFreq[_band - 1] < freq && freq < crossFreq[_band + 1]) {
            if (freq == 0) freq = 1;
            crossFreq[_band] = freq;
            band[0].setBiquad(bq_type_lowpass, crossFreq[1] / (float)sampleRate, 0.7071, -3);

            float fPeak = (crossFreq[2] + crossFreq[1]) / 2;
            float bandWidth = crossFreq[2] - crossFreq[1];
            float Q = fPeak / bandWidth;
            band[1].setBiquad(bq_type_bandpass, fPeak / (float)sampleRate, Q, -3);

            fPeak = (crossFreq[3] + crossFreq[2]) / 2;
            bandWidth = crossFreq[3] - crossFreq[2];
            Q = fPeak / bandWidth;
            band[2].setBiquad(bq_type_bandpass, fPeak / (float)sampleRate, Q, -3);

            band[3].setBiquad(bq_type_highpass, crossFreq[3] / (float)sampleRate, 0.7071, -3);
        }
    }

    return crossFreq[_band];
}

float Crossover::setVolume(int _band, float _volume) {
    if (0 <= _volume && _volume <= 100)
        volume[_band] = _volume;

    return volume[_band];
}